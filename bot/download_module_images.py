#!/usr/bin/env python
import os

import requests
from bs4 import *
from icecream import ic
from PIL import Image

folder_name = "images"
NEW_WIDTH = 90


# CREATE FOLDER
def folder_create():
    try:
        os.mkdir(folder_name)
    except FileExistsError:
        pass


# DOWNLOAD ALL IMAGES FROM THAT URL
def download_images(images, folder_name):

    # initial count is zero
    count = 0
    downloaded = []

    # print total images found in URL
    print(f"Total {len(images)} Image Found!")

    # checking if images is not zero
    if len(images) != 0:
        for i, image in enumerate(images):
            image_link = None
            # From image tag ,Fetch image Source URL

            # 1.data-srcset
            # 2.data-src
            # 3.data-fallback-src
            # 4.src

            # Here we will use exception handling

            # first we will search for "data-srcset" in img tag
            try:
                # In image tag ,searching for "data-srcset"
                image_link = image["data-srcset"]

            # then we will search for "data-src" in img
            # tag and so on..
            except KeyError:
                try:
                    # In image tag ,searching for "data-src"
                    image_link = image["data-src"]
                except KeyError:
                    try:
                        # In image tag ,searching for "data-fallback-src"
                        image_link = image["data-fallback-src"]
                    except KeyError:
                        try:
                            # In image tag ,searching for "src"
                            image_link = image["src"]

                        # if no Source URL found
                        except KeyError:
                            pass
            if image_link is not None:
                try:
                    target = image["data-image-key"].lower()

                    # target = image["data-image-name"]
                    # After getting Image Source URL
                    # We will try to get the content of image
                    try:
                        if (
                            ("asset" not in image_link)
                            and ("portrait" not in image_link)
                            and ("png" in image_link)
                        ):
                            r = requests.get(image_link).content
                            try:

                                # possibility of decode
                                r = str(r, "utf-8")

                            except UnicodeDecodeError:

                                # After checking above condition, Image Download start
                                with open(f"{folder_name}/{target}", "wb+") as f:
                                    f.write(r)
                                downloaded.append(f"{folder_name}/{target}")

                                # counting number of image downloaded
                                count += 1
                    except:
                        pass

                except:
                    pass

        # There might be possible, that all
        # images not download
        # if all images download
        if count == len(images):
            print("All Images Downloaded!")

        # if all images not download
        else:
            print(f"Total {count} Images Downloaded Out of {len(images)}")
    return downloaded


def resize_and_center_image(image_path):
    with Image.open(image_path) as img:
        width, height = img.size  # Get dimensions
        left = round((NEW_WIDTH - width) / 2)
        top = round((NEW_WIDTH - height) / 2)
        bg = Image.new("RGBA", (NEW_WIDTH, NEW_WIDTH))
        bg.paste(img, (left, top))
    bg.save(image_path)


# MAIN FUNCTION START
def main():
    downloaded = []
    # Call folder create function
    folder_create()

    for url in [
        "https://hades-star.fandom.com/wiki/Support_Modules",
        "https://hades-star.fandom.com/wiki/Trade_Modules",
        "https://hades-star.fandom.com/wiki/Shield_Modules",
        "https://hades-star.fandom.com/wiki/Mining_Modules",
        "https://hades-star.fandom.com/wiki/Weapon_Modules",
    ]:
        # content of URL
        r = requests.get(url)

        # Parse HTML Code
        soup = BeautifulSoup(r.text, "html.parser")

        # find all images in URL
        images = soup.findAll("img")

        downloaded.extend(download_images(images, folder_name))
    # ic(downloaded)
    bg = Image.new("RGBA", (NEW_WIDTH, NEW_WIDTH))
    bg.save("images/blank.png")
    for image in downloaded:
        resize_and_center_image(image)


# take url

main()
