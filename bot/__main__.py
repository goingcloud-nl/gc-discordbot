# this is the main bot file
import main

if __name__ == "__main__":
    """Main bot!"""
    main = main.Main()
    settings = main._get_env_settings()
    bot = main._run_bot(settings)
