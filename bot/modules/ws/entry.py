"""
All related to whitestar functionality
"""
import locale

from discord.ext import commands
from loguru import logger

from ..utils import feedback, get_channel_by_name, get_role_id, in_role, sanitize

try:
    locale.setlocale(locale.LC_ALL, "nl_NL.utf8")  # required running on linux
    logger.info("running on linux")
except locale.Error:
    locale.setlocale(locale.LC_ALL, "nl_NL.UTF-8")  # required when running on MAC
    logger.info("running on mac")


########################################################################################
#  function update_ws_inschrijvingen_tabel
########################################################################################
async def update_ws_inschrijvingen_tabel(bot, db, wslist_channel):
    """
    This wil write the list of "inschrijvingen" to the wslist_channel,
    it is based on the contents of the sqlite table
    """

    # Get all subscribers for the ws
    get_entries = (
        db.session.query(
            db.WSEntry.EntryType,
            db.WSEntry.Remark,
            db.User.DiscordAlias,
        )
        .filter(db.WSEntry.Active)
        .join(db.User)
        .order_by(db.WSEntry.EntryType)
        .order_by(db.WSEntry.EntryTime)
    )
    msg = ""
    i = 1
    for item in get_entries.all():
        logger.info(f"item {item}")
        if item.EntryType == "planner":
            msg += f"**{i}. {item.DiscordAlias} {item.EntryType} {item.Remark}**\n"
        else:
            msg += f"{i} {item.DiscordAlias} {item.EntryType} {item.Remark}\n"
        i += 1
    msg += "\n"
    num_planners = (
        db.session.query(db.WSEntry)
        .filter(db.WSEntry.Active)
        .filter(db.WSEntry.EntryType == "planner")
        .count()
    )
    num_players = (
        db.session.query(db.WSEntry)
        .filter(db.WSEntry.Active)
        .filter(db.WSEntry.EntryType == "speler")
        .count()
    )

    logger.info(f"num_players: {num_players}, num_planners: {num_planners}")
    msg += (
        f"**Planners:** {num_planners}, "
        f"**Spelers:** {num_players}, "
        f"**Totaal:** {num_planners+num_players}"
    )
    msg += "\n"

    async for message in wslist_channel.history(limit=20):
        if message.author == bot.user:
            logger.debug(f"going to delete message for {message.author} == {bot.user}")
            await message.delete()
    await wslist_channel.send(msg)


########################################################################################
#  function _ws_entry
########################################################################################


async def _ws_entry(
    self, db, bot, ctx: commands.Context = None, action: str = "", comment: str = ""
):
    """
    Handle the entry for the ws
    """
    usermap = self._getusermap(str(ctx.author.id))

    settings = dict(bot.settings)
    wsin_channel = get_channel_by_name(ctx, settings.get("WSIN_CHANNEL_NAME"))
    wslist_channel = get_channel_by_name(ctx, settings.get("WSLIST_CHANNEL_NAME"))

    is_entered = (
        db.session.query(db.WSEntry)
        .filter(db.WSEntry.Active)
        .filter(db.WSEntry.UserId == usermap["UserId"])
        .count()
    )
    logger.info(f"is_entered: {is_entered}")
    logger.info(f"{usermap['DiscordAlias']} heeft als action: {action}")
    if action == "out":
        if is_entered == 0:
            logger.info(f"{usermap['DiscordAlias']} stond nog niet ingeschreven.. ")
            msg = f"{usermap['DiscordAlias']}, je stond nog niet ingeschreven.. "
            await feedback(ctx=ctx, msg=msg, delete_after=3)
        else:
            db.session.query(db.WSEntry).where(db.WSEntry.Active == True).where(
                db.WSEntry.UserId == usermap["UserId"]
            ).delete()
            logger.info(f"{usermap['DiscordAlias']} stond wel ingeschreven.. ")
            msg = f"Helaas, {usermap['DiscordAlias']} je doet niet mee met komende ws"
            await feedback(ctx=ctx, msg=msg, delete_after=3)

            await update_ws_inschrijvingen_tabel(
                db=db, bot=bot, wslist_channel=wslist_channel
            )

            async for message in wsin_channel.history(limit=50):
                if message.author.id == ctx.author.id:
                    logger.info(f"deleting message for {message.author.id}")
                    await message.delete()
        return None
    rows_same_role = (
        db.session.query(db.WSEntry)
        .filter(db.WSEntry.Active)
        .filter(db.WSEntry.UserId == usermap["UserId"])
        .filter(db.WSEntry.EntryType == action)
        .count()
    )
    logger.info(f"rows_same_role {rows_same_role}")
    if rows_same_role == 1:
        # already registerd with the same role, do nothing..
        await ctx.send(f"{usermap['DiscordAlias']} is al ingeschreven als {action}")
        return None
    if is_entered == 1:
        logger.info("updating")
        # already registerd as a different role, update
        data = {"EntryType": action, "Remark": comment}
        db.session.query(db.WSEntry).filter(db.WSEntry.Active == True).filter(
            db.WSEntry.UserId == usermap["UserId"]
        ).update(data)
    else:
        logger.info("adding")
        # not yet registerd, insert
        new_entry = db.WSEntry(
            UserId=usermap["UserId"], EntryType=action, Remark=comment, Active=True
        )
        db.session.add(new_entry)
    await ctx.send(
        content=(
            f"Gefeliciteerd, {usermap['DiscordAlias']} "
            f"je bent nu {action} voor de volgende ws"
        ),
        delete_after=3,
    )
    db.session.commit()


########################################################################################
#  function _ws_admin
########################################################################################


async def _ws_admin(bot, db, ctx, action: str):
    """
    execute administrative tasks for the WS entry
    """
    settings = dict(bot.settings)
    wsin_channel = get_channel_by_name(ctx, settings.get("WSIN_CHANNEL_NAME"))
    wslist_channel = get_channel_by_name(ctx, settings.get("WSLIST_CHANNEL_NAME"))
    ws_role = ctx.guild.get_role(get_role_id(ctx, settings.get("WS_ROLE_NAME")))

    if not (in_role(ctx, "Moderator") or in_role(ctx, "Bot Bouwers")):
        await feedback(
            ctx=ctx, msg="You are not an admin", delete_after=5, delete_message=True
        )
        return None

    if action == "open":
        await wsin_channel.set_permissions(ws_role, send_messages=True)
        await ctx.send(content=f"Inschrijving geopend door {ctx.author.name}")
    elif action == "close":
        await wsin_channel.set_permissions(ws_role, send_messages=False)
        await ctx.send(
            content=(
                f"Inschrijving gesloten door {ctx.author.name}\n"
                "Je kan verwachten dat begin van volgende week de inschrijvingen"
                " worden geopend Ma,di of Wo.\n"
            )
        )
    elif action == "clear":
        msg = (
            f"{ws_role.mention}, De WS inschrijving is geopend\n"
            "Je kan verwachten dat begin van de week de inschrijvingen worden geopend"
            " Ma,di of Wo.\n\n"
            "Met `!ws plan` of `!ws p` schrijf je je in als planner en speler\n"
            "Met `!ws in` of `!ws i` schrijf je je in als speler\n\n\n"
            f"Inschrijven kan alleen in {wsin_channel.mention},\n"
            f" het overzicht van de inschrijvingen komt in {wslist_channel.mention},\n"
            "met __30 inschrijvingen worden er 2 wsen__ gestart,\n"
            " op voorwaarde van minimaal **5 planners** zijn.\n"
            " (Je kan ook aangwezen worden)\n\n\n"
            "Als er op de *dag van opening 15 inschrijvingen* zijn **sluiten we de"
            " inschrijving**, bij meer of minder inschrijvingen wordt de inschrijving"
            " __24uur verlengd.__"
        )
        await wsin_channel.purge(limit=100)
        await wslist_channel.purge(limit=100)
        await ctx.send(content=msg)
        data = {"Active": False}
        db.session.query(db.WSEntry).update(data)
        await update_ws_inschrijvingen_tabel(
            db=db, bot=bot, wslist_channel=wslist_channel
        )
        await wsin_channel.set_permissions(ws_role, send_messages=True)
        db.session.commit()
        return None


########################################################################################
#  command ws  (inschrijvingen)
#######################################################################################


@commands.command(
    name="ws",
    help=(
        "Met het ws commando schrijf je je in (of uit) voor de volgende ws, opties:\n"
        " plan/p [opmerking] - aanmelden als planner voor de volgende ws\n"
        " in/i [opmerking]   - aanmelden als speler voor de volgende ws\n"
        " uit/u              - afmelden voor de volgende ws (als je aangemeld was)\n"
        "\n"
        "\n"
        "Inschrijven kan **alleen** in het #ws-inschrijvingen kanaal. Het overzicht"
        "kom in #ws-inschrijflijst. Updaten van je rol (speler -> planner) kan door je"
        "in te schrijven met je nieuwe rol.\n"
        "inschrijven planner met !ws plan\n"
        "inschrijven als speler met !ws in\n"
        "uitschrijven kan met !ws out\n"
        "\n"
        "Onderstaande opties zijn voor Moderator only:\n"
        " open  - open het ws-inschrijvingen kanaal\n"
        " close - sluit het ws-inschrijvingen kanaal\n"
        " clear - schoon het ws-inschrijvingen kanaal, inschrijvingen worden geopend.\n"
    ),
    brief="Schrijf jezelf in voor de volgende ws",
)
async def ws(self, ctx, *args):
    """
    The function to handle the ws inschrijvingen related stuff
    """
    usermap = self._getusermap(str(ctx.author.id))
    settings = dict(self.bot.settings)
    wsin_channel = get_channel_by_name(ctx, settings.get("WSIN_CHANNEL_NAME"))
    wslist_channel = get_channel_by_name(ctx, settings.get("WSLIST_CHANNEL_NAME"))

    comment = ""
    if ctx.channel != wsin_channel:
        # Trying to post in the wrong channel
        msg = (
            f"{usermap['DiscordAlias']}, je kunt alleen in kanaal <#{wsin_channel.id}> "
            "inschrijven, je bent nu nog **niet** ingeschreven!"
        )

        await feedback(ctx=ctx, msg=msg, delete_after=3, delete_message=True)
        return None
    if len(args) == 0:
        # no arguments, send help!
        await ctx.send_help(ctx.command)
        return None

    if len(args) > 1:
        # more than 1 argument, join
        comment = sanitize(" ".join(args[1:]))

    logger.info(f"{usermap['DiscordAlias']} - {args[0]} - {comment}")
    if args[0] in ["i", "in"]:
        await self._ws_entry(
            db=self.db, bot=self.bot, ctx=ctx, action="speler", comment=comment
        )
    elif args[0] in ["p", "plan", "planner"]:
        await self._ws_entry(
            db=self.db, bot=self.bot, ctx=ctx, action="planner", comment=comment
        )
    elif args[0] in ["u", "uit", "o", "out"]:
        await self._ws_entry(
            db=self.db, bot=self.bot, ctx=ctx, action="out", comment=comment
        )
    elif args[0] in ["close", "sluit"]:
        await _ws_admin(db=self.db, bot=self.bot, ctx=ctx, action="close")
    elif args[0] in ["open"]:
        await _ws_admin(db=self.db, bot=self.bot, ctx=ctx, action="open")
    elif args[0] in ["clear"]:
        await _ws_admin(db=self.db, bot=self.bot, ctx=ctx, action="clear")
    else:
        await ctx.send("Ongeldige input")
        await ctx.send_help(ctx.command)
        return None

    await update_ws_inschrijvingen_tabel(
        bot=self.bot, db=self.db, wslist_channel=wslist_channel
    )


########################################################################################
#  command updateusermap
########################################################################################


@commands.command(
    name="updateusermap",
    help=("Moderator only, geen argumenten, update de usermap tabel\n"),
    brief="Update de usermap tabel",
    hidden="True",
)
async def updateusermap(self, ctx):
    """
    Get the mapping for discordalias and gsheetalias
    Id is the key for the selection.
    If Id is not yet in usermap table it will be added
    with the provided alias.
    """
    if in_role(ctx, "Moderator") or in_role(ctx, "Bot Bouwers"):

        guild = ctx.guild
        members = guild.members

        for member in members:
            if (
                self.db.session.query(self.db.User)
                .filter(self.db.User.UserId == member.id)
                .count()
                == 0
            ):

                new_user = self.db.User(
                    UserId=member.id, DiscordAlias=member.display_name
                )
                self.db.session.add(new_user)
                self.db.session.commit()
                logger.info(f"inserted {member.display_name}")
        await ctx.send(f"user table updated by {ctx.author.name}")
