"""

"""
from datetime import datetime

from loguru import logger


def _update_gsheet_entry(self, item):
    try:
        item[2] = datetime.strptime(item[2], "%d-%m-%Y")

        if self.db.session.query(self.db.Gsheet).filter_by(Naam=item[1]).count() != 0:
            self.db.session.query(self.db.Gsheet).filter_by(Naam=item[1]).delete()

        new_item = self.db.Gsheet(
            Influence=int(item[0]),
            Naam=str(item[1]),
            Bijgewerkt=item[2],
            CredCap=str(item[3]),
            Transport=str(item[4]),
            TotalCargoSlots=int(item[5]),
            Miner=str(item[6]),
            HydrogenCapacity=int(item[7]),
            Battleship=str(item[8]),
            CargoBayExtension=int(item[9]),
            ShipmentComputer=int(item[10]),
            TradeBoost=int(item[11]),
            Rush=int(item[12]),
            TradeBurst=int(item[13]),
            ShipmentDrone=int(item[14]),
            Offload=int(item[15]),
            ShipmentBeam=int(item[16]),
            Entrust=int(item[17]),
            Dispatch=int(item[18]),
            Recall=int(item[19]),
            RelicDrone=int(item[20]),
            MiningBoost=int(item[21]),
            HydrogenBayExtension=int(item[22]),
            Enrich=int(item[23]),
            RemoteMining=int(item[24]),
            HydrogenUpload=int(item[25]),
            MiningUnity=int(item[26]),
            Crunch=int(item[27]),
            Genesis=int(item[28]),
            HydrogenRocket=int(item[29]),
            MiningDrone=int(item[30]),
            WeakBattery=int(item[31]),
            Battery=int(item[32]),
            Laser=int(item[33]),
            MassBattery=int(item[34]),
            DualLaser=int(item[35]),
            Barrage=int(item[36]),
            DartLauncher=int(item[37]),
            AlphaShield=int(item[38]),
            DeltaShield=int(item[39]),
            PassiveShield=int(item[40]),
            OmegaShield=int(item[41]),
            MirrorShield=int(item[42]),
            BlastShield=int(item[43]),
            AreaShield=int(item[44]),
            EMP=int(item[45]),
            Teleport=int(item[46]),
            RedStarLifeExtender=int(item[47]),
            RemoteRepair=int(item[48]),
            TimeWarp=int(item[49]),
            Unity=int(item[50]),
            Sanctuary=int(item[51]),
            Stealth=int(item[52]),
            Fortify=int(item[53]),
            Impulse=int(item[54]),
            AlphaRocket=int(item[55]),
            Salvage=int(item[56]),
            Suppress=int(item[57]),
            Destiny=int(item[58]),
            Barrier=int(item[59]),
            Vengeance=int(item[60]),
            DeltaRocket=int(item[61]),
            Leap=int(item[62]),
            Bond=int(item[63]),
            LaserTurret=int(item[64]),
            AlphaDrone=int(item[65]),
            Suspend=int(item[66]),
            OmegaRocket=int(item[67]),
            RemoteBomb=int(item[68]),
        )
        self.db.session.add(new_item)
        self.db.session.commit()
        return True
    except ValueError:
        logger.debug(f"something went wrong with {item[1]}")
        return False
