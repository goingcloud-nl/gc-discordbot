"""

"""
import gspread
from discord.ext import commands

gsheet_url = "https://docs.google.com/spreadsheets/d/1tCwYgWGwbYViWKpdTCJSMPiQVEBCnoQvryqgN0DMcI0/"


@commands.command(
    name="update_gsheet",
    help="Update de google sheet naar Robin (alleen voor admins)",
    brief="Update de google sheet naar Robin (alleen voor admins)",
)
async def update_gsheet(self, ctx):
    updated = 0
    failed = 0
    if self._is_admin(ctx):
        gc = gspread.service_account()
        gsheet = gc.open_by_url(gsheet_url)
        worksheet = gsheet.worksheet("Mods")
        list_of_lists = worksheet.get_all_values()
        lijstjes = list_of_lists[1:]
        for lijstje in lijstjes:
            if lijstje[0] != "100":
                lijstje.pop(5)
                lijstje.pop(4)
                if self._update_gsheet_entry(lijstje):
                    updated = updated + 1
                else:
                    failed = failed + 1
        await self._feedback(ctx=ctx, msg=f"Gegevens van {updated} users geupdate.")
        await self._feedback(ctx=ctx, msg=f"Updaten van {failed} is mislukt.")

    else:
        await self._feedback(ctx=ctx, msg="Tsss. je bent helemaal geen admin...")
