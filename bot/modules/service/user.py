"""
The contents of this file is to reflect happyness of robin.
"""
from discord.ext import commands


async def _update_gsheet_alias(db, userid, alias):
    alias_exists = db.session.query(db.Gsheet).filter(db.Gsheet.Naam == alias).count()
    if alias_exists == 1:
        alias_data = {"GsheetAlias": alias}
        db.session.query(db.User).filter(db.User.UserId == userid).update(alias_data)
        db.session.commit()
        return True
    else:
        return False


@commands.command(
    name="gsalias",
    brief="Update google sheet alias",
    help=(
        "Update jouw google sheet alias voor Robin, het commando hiervoor is "
        "`gsalias aliasnaam`. Als je admin bent kun je het ook voor een andere user "
        "doen met `gsalias discordnaam/discordid aliasnaam`."
    ),
)
async def gsalias(self, ctx, *args):
    """
    gsalias <alias_name>
    gsalias user/userid <alias_name>
    """
    if len(args) == 1:
        alias_exists = await _update_gsheet_alias(
            db=self.db, userid=str(ctx.author.id), alias=args[0]
        )
        if alias_exists:
            await self._feedback(ctx=ctx, msg=f"Google alias is set to **{args[0]}**")
        else:
            await self._feedback(
                ctx=ctx, msg=f"**{args[0]}** was not found in the google sheet"
            )

    elif len(args) == 2:
        if self._is_admin(ctx):
            userid = self._get_userid(db=self.db, user=args[0])
            if userid is not None:
                alias_exists = await _update_gsheet_alias(
                    db=self.db, userid=userid, alias=args[1]
                )
                if alias_exists:
                    await self._feedback(
                        ctx=ctx,
                        msg=(
                            f"Google alias is set to **{args[1]}**"
                            f" for **{self._get_username_by_id(userid)}**"
                        ),
                    )
                else:
                    await self._feedback(
                        ctx=ctx, msg=f"**{args[1]}** niet gevonden in de google sheet"
                    )
            else:
                await self._feedback(
                    ctx=ctx, msg="discordnaam en/of discordid niet gevonden..."
                )
        else:
            await self._feedback(ctx=ctx, msg="Tsss. je bent helemaal geen admin...")
    else:
        await self._feedback(
            ctx=ctx, msg="Probeer het nog een keer, maar dan met de juiste syntax.."
        )
