"""
All related to members
"""
from discord.ext import commands

from ..robin import Robin


class Service(Robin):
    from ._update_gsheet_entry import _update_gsheet_entry
    from ._zapier_update import _zapier_update
    from .update_gsheet import update_gsheet
    from .user import gsalias

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.channel.name == "zapier-melding" and (
            message.author.name == "Zapier"
            or message.author.name == "xray"
            or message.author.name == "Xray"
        ):
            # await self._repost_zapier_message(message)
            await self._zapier_update(message)
