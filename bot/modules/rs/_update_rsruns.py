import math
from datetime import datetime, timedelta

from discord.ext import commands, tasks
from icecream import ic
from sqlalchemy import func

# inserting rs run into table
# create waiter for 30mins
# create "please update rsscore met !rscore #id scrore"
# update table met score
# update table in channel


def _add_rsruns_list_entry(self, level):
    new_entry = self.db.RSRunList(
        Level=level, Runtime=datetime.now().replace(second=0, microsecond=0)
    )
    self.db.session.add(new_entry)
    self.db.session.commit()
    print(f"new_entry.RunId {new_entry.RunId}")
    return new_entry.RunId


@commands.command(
    name="score",
    help=(
        "Met het status commando update je status in het status kanaal,"
        " hiermee help je je mede ws-ers op de hoogte te houden hoe snel je kunt reageren."
    ),
    brief="Update je status in het status kanaal",
)
async def score(self, ctx, *args):
    # !score 7 12312
    runid = args[0]
    score = args[1]
    ic(runid)
    ic(score)
    players = (
        self.db.session.query(self.db.RSEvent.DiscordId)
        .filter(self.db.RSEvent.RunId == runid)
        .all()
    )
    number_players = len(players)
    ic(ctx.author.id)
    ic(players)
    ic(number_players)
    player_ids = [608615397704597520, 332920940675727370]
    for item in players:
        player_ids.append(int(item.DiscordId))
    ic(player_ids)
    if int(ctx.author.id) not in player_ids:
        ic("found")
        await self._feedback(
            ctx=ctx,
            msg=f"Je deed niet mee met RunID {runid}, je kunt hierop geen score invullen",
        )
    else:
        ic("not found")
        float_score_pp = int(score) / number_players
        ic(float_score_pp)
        score_pp = math.floor(float_score_pp + 0.5)
        ic(score_pp)
        data = {"Score": score, "Score_PP": score_pp}
        self.db.session.query(self.db.RSRunList).filter(
            self.db.RSRunList.RunId == runid
        ).update(data)

        self.db.session.commit()
        await self._update_rsevent_table()
        await self._feedback(
            ctx=ctx, msg=f"Dank {ctx.author.name}, de score is verwerkt"
        )
        # data = {"Active": False}
        # self.db.session.query(self.db.WSEntry).update(data)


async def _notify_runnummer(self, runid, level, player_ids):
    message_channel = self._get_channel_by_name("event")
    get_player_names = self.db.session.query(self.db.User.DiscordAlias).filter(
        self.db.User.UserId.in_(player_ids)
    )
    ic(get_player_names)
    msg = f"RunID {runid} (rs{level}): "
    for row in get_player_names.all():
        msg = " ".join([msg, row.DiscordAlias])
    await message_channel.send(msg)


async def _update_rsruns(self, content):
    text = content.split()
    for i, item in enumerate(text):
        print(f"{i} __{item}__")

    if len(text) > 1 and "start" in text[1]:
        player_ids = []
        level = text[0].replace("rs", "")
        runid = self._add_rsruns_list_entry(level)
        ic(runid)
        for line in text[2:6]:
            if "<@" in line:
                user = line.replace("<@", "").replace(">", "").replace("!", "")
                ic(user)
                ic(runid)
                player_ids.append(user)
                new_entry = self.db.RSEvent(DiscordId=user, RSLevel=level, RunId=runid)
                self.db.session.add(new_entry)
        self.db.session.commit()
        await self._notify_runnummer(runid, level, player_ids)


###############################################################################################
#  runner return_scheduler
###############################################################################################
@tasks.loop(minutes=1)
async def score_updater(self):
    """
    this is the "cron" for the comeback notifications
    """
    message_channel = self._get_channel_by_name("event")
    diffmin = 30
    compare_datetime = datetime.now().replace(second=0, microsecond=0) - timedelta(
        minutes=diffmin
    )
    ic(compare_datetime)
    get_score = (
        self.db.session.query(self.db.RSRunList.RunId, self.db.RSEvent.DiscordId)
        .filter(self.db.RSRunList.Runtime == compare_datetime)
        .filter(self.db.RSRunList.Score == 0)
        .filter(
            self.db.RSEvent.Runtime.between(
                datetime.strptime(self.rsevent_starttime, "%Y-%m-%d %H:%M"),
                datetime.strptime(self.rsevent_endtime, "%Y-%m-%d %H:%M"),
            )
        )
        .join(self.db.RSEvent, self.db.RSEvent.RunId == self.db.RSRunList.RunId)
    )
    msg = ""
    runid = None
    for item in get_score.all():
        msg = " ".join([msg, f"<@{str(item.DiscordId)}>"])
        ic(item)
        runid = item.RunId
    if runid is not None:
        await message_channel.send(
            f"{msg}, kan 1 van jullie de score van run {runid} invullen? "
            "(hoeft maar 1 keer per run)\n"
            f"dit kan door middel van ```{self.bot.command_prefix}score {runid} <score>```\n"
        )
