"""
The contents of this file is to reflect happyness of robin.
"""
from datetime import datetime

from icecream import ic
from sqlalchemy import func


def _get_dbdict_combined(self) -> list:
    # select x.name, x.nr_runs, y.score from (
    # SELECT u.DiscordAlias as name, count(1) as nr_runs
    # FROM rsevent r
    # inner join "user" u on u.UserId = r.DiscordId
    # where r.runtime between strftime('%Y-%m-%d %H:%M', "2022-02-14 18:00") and strftime('%Y-%m-%d %H:%M', "2022-02-21 18:00")
    # group by r.DiscordId
    # ) as x
    # inner join (
    # select u.DiscordAlias as name, sum(rl.Score_PP) as score
    # from "rsrun-list" rl
    # inner join rsevent rs  on rl.RunId = rs.RunId
    # inner join "user" u on u.UserId = rs.DiscordId
    # where rl.runtime  between strftime('%Y-%m-%d %H:%M', "2022-02-14 18:00") and strftime('%Y-%m-%d %H:%M', "2022-02-21 18:00")
    # group by rs.DiscordId
    # ) as y on x.name = y.name
    # order by score desc
    subquery_by_count = (
        self.db.session.query(
            self.db.User.DiscordAlias.label("name"), func.count("*").label("run_count")
        )
        .join(self.db.RSEvent, self.db.RSEvent.DiscordId == self.db.User.UserId)
        .filter(
            self.db.RSEvent.Runtime.between(
                datetime.strptime(self.rsevent_starttime, "%Y-%m-%d %H:%M"),
                datetime.strptime(self.rsevent_endtime, "%Y-%m-%d %H:%M"),
            )
        )
        .group_by(self.db.User.DiscordAlias)
    ).subquery()
    subquery_by_score = (
        self.db.session.query(
            self.db.User.DiscordAlias.label("name"),
            func.sum(self.db.RSRunList.Score_PP).label("score"),
        )
        .join(self.db.RSEvent, self.db.RSEvent.DiscordId == self.db.User.UserId)
        .join(self.db.RSRunList, self.db.RSEvent.RunId == self.db.RSRunList.RunId)
        .filter(
            self.db.RSEvent.Runtime.between(
                datetime.strptime(self.rsevent_starttime, "%Y-%m-%d %H:%M"),
                datetime.strptime(self.rsevent_endtime, "%Y-%m-%d %H:%M"),
            )
        )
        .group_by(self.db.User.DiscordAlias)
    ).subquery()
    get_status = (
        self.db.session.query(
            subquery_by_score.c.name,
            subquery_by_count.c.run_count,
            subquery_by_score.c.score,
        )
        .join(subquery_by_count, subquery_by_score.c.name == subquery_by_count.c.name)
        .order_by(subquery_by_score.c.score.desc())
    )
    status_list = []
    for item in get_status.all():
        status_list.append(item)
        # status_dict[item.DiscordAlias] = {"count": item.run_count}
    return status_list


def _get_dbdict_by_count(self) -> list:
    get_status = (
        self.db.session.query(
            self.db.User.DiscordAlias, func.count("*").label("run_count")
        )
        .join(self.db.RSEvent, self.db.RSEvent.DiscordId == self.db.User.UserId)
        .filter(
            self.db.RSEvent.Runtime.between(
                datetime.strptime(self.rsevent_starttime, "%Y-%m-%d %H:%M"),
                datetime.strptime(self.rsevent_endtime, "%Y-%m-%d %H:%M"),
            )
        )
        .group_by(self.db.User.DiscordAlias)
        .order_by(func.count("*").desc())
    )
    status_list = []
    for item in get_status.all():
        status_list.append(item)
        # status_dict[item.DiscordAlias] = {"count": item.run_count}
    return status_list


def _get_dbdict_by_score(self) -> list:
    get_status = (
        self.db.session.query(
            self.db.User.DiscordAlias,
            func.sum(self.db.RSRunList.Score_PP).label("score"),
        )
        .join(self.db.RSEvent, self.db.RSEvent.DiscordId == self.db.User.UserId)
        .join(self.db.RSRunList, self.db.RSEvent.RunId == self.db.RSRunList.RunId)
        .filter(
            self.db.RSEvent.Runtime.between(
                datetime.strptime(self.rsevent_starttime, "%Y-%m-%d %H:%M"),
                datetime.strptime(self.rsevent_endtime, "%Y-%m-%d %H:%M"),
            )
        )
        .group_by(self.db.User.DiscordAlias)
        .order_by(func.sum(self.db.RSRunList.Score_PP).desc())
    )
    # status_dict = {}
    # for item in get_status.all():
    #     status_dict[item.DiscordAlias] = {"score": item.score}
    status_list = []
    for item in get_status.all():
        status_list.append(item)
        # status_dict[item.DiscordAlias] = {"count": item.run_count}
    return status_list


async def _update_rsevent_table(self):
    """
    updating status table in status channel
    """
    eventlist_channel = self._get_channel_by_name("event-punten")
    msg = (
        "Overzicht van de RS score van iedereen.\n"
        f"gedaan heeft, van dit event lopend van {self.rsevent_starttime} tot {self.rsevent_endtime}\n\n"
        "```"
    )
    db_dict = self._get_dbdict_combined()
    # db_dict_by_count = self._get_dbdict_by_count()
    # db_dict = _combine_dbdict(db_dict_by_score, db_dict_by_count)
    ic(db_dict)
    msg += f"{'':>2}  {'speler':24} {'runs':>3}  {'score':7} \n"
    for index, item in enumerate(db_dict):
        ic(msg)
        # stripped_name = item.name.replace('[🥩]', '').replace('🍻', '').replace('🔆', '').replace('|','')
        stripped_name = item.name.replace("[🥩]", "")
        stripped_name = stripped_name.replace("🍻", "")
        stripped_name = stripped_name.replace("🔆", "")
        stripped_name = stripped_name.replace("|", "")
        msg += f"{index+1:>2}. {stripped_name:25} {item.run_count:>2} {item.score:7} \n"
    msg += "```"
    await eventlist_channel.purge()
    await eventlist_channel.send(msg)
