"""
All related to whitestar functionality
"""
from datetime import datetime

from discord.ext import commands

from ..robin import Robin

RSEVENT_STARTIME = "2024-03-16 01:00"
RSEVENT_ENDTIME = "2024-03-18 01:00"
date_format = "%Y-%m-%d %H:%M"


class RS(Robin):
    from ._update_rsevent_table import (
        _get_dbdict_by_score,
        _get_dbdict_combined,
        _update_rsevent_table,
    )
    from ._update_rsruns import (
        _add_rsruns_list_entry,
        _notify_runnummer,
        _update_rsruns,
        score,
        score_updater,
    )
    from .rsevent import rsevent

    def __init__(self, bot=None, db=None, static=None):
        super().__init__(bot=bot, db=db)
        self.rsevent_starttime = RSEVENT_STARTIME
        self.rsevent_endtime = RSEVENT_ENDTIME
        self.static = static
        self.score_updater.start()

    @commands.Cog.listener()
    async def on_message(self, message):
        rsevent_start = datetime.strptime(RSEVENT_STARTIME, date_format)
        nu = datetime.now()
        rsevent_end = datetime.strptime(RSEVENT_ENDTIME, date_format)

        if rsevent_start < nu < rsevent_end:
            if (
                message.channel == self._get_channel_by_name("runs")
                and message.author.name == "RedStarQueueBot"
            ):  # this can be used when the rs event is active
                await self._update_rsruns(message.content)
