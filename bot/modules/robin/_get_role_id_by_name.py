from loguru import logger

# TODO: need to rewrite _get_role_id_by_name is copied from old setup.


def _get_role_id_by_name(self, role_name: str):
    """
    Get the channel for a channel_name.
    paramters:
        channel_name:        The channel where to fetch channel for.
    """
    all_roles = self.static.get("all_guild_roles")
    for role in all_roles:
        if role.get("name") == role_name:
            return role.get("id")
    logger.info("role_not_found")
    return "role_not_found"
