def _has_gsalias(self, userid):
    result = (
        self.db.session.query(self.db.Gsheet)
        .join(self.db.User, self.db.Gsheet.Naam == self.db.User.GsheetAlias)
        .filter(self.db.User.UserId == userid)
    )
    if result.count() == 1:
        return True
    else:
        return False
