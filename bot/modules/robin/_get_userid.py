def _get_userid(self, db, user):
    userid = None
    if user.isdigit():
        result = db.session.query(db.User).filter(db.User.UserId == user).count()
        if result == 1:
            userid = user
    else:
        result = db.session.query(db.User).filter(
            db.User.DiscordAlias.ilike(user + "%")
        )
        if result.count() == 1:
            userid = result.all()[0].UserId
    return userid
