""" Display module info about a Discord User in a graph
"""
from math import floor as math_floor

from discord.commands import Option, slash_command
from discord.commands.context import ApplicationContext
from discord.ext import commands
from icecream import ic


async def info(self, ctx: ApplicationContext, userid: int):
    """Get module info about a user

    Args:
        ctx (ApplicationContext): Discord Context
        userid (int): userid which maps to the database
    """
    row = (
        self.db.session.query(self.db.User).filter(self.db.User.UserId == userid).one()
    )

    if row.LastActive is None:
        last_active = "Al even niet actief.."
        last_channel = "niet bekend"
    else:
        last_active = row.LastActive.strftime("%d/%m/%Y, %H:%M:%S")
        last_channel = row.LastChannel
    msg = (
        f"Gsheet Alias: {row.GsheetAlias}\n"
        f"Discord Id: {row.UserId}\n"
        f"Laatst actief op discord: {last_active}\n"
        f"Laatst actief in kanaal: {last_channel}\n"
    )

    if not self._has_gsalias(userid):
        await self._feedback(ctx=ctx, msg=msg)
        msg = (
            f"Geen google alias gevonden voor {row.DiscordAlias}, "
            " je kunt deze zetten met `!gsalias <google aliasnaam>`"
        )
        await self._feedback(ctx=ctx, msg=msg)
    else:
        gsheet_row = (
            self.db.session.query(self.db.Gsheet)
            .filter(self.db.Gsheet.Naam == row.GsheetAlias)
            .one()
        )
        sheet_bijgewerkt = gsheet_row.Bijgewerkt.strftime("%d/%m/%Y")
        msg = msg + (f"Google Sheet bijgewerkt: {sheet_bijgewerkt}\n")
        msg = msg + (f"Hydro Capacity: {gsheet_row.HydrogenCapacity}\n")
        msg = msg + (
            f"Total Cargo slots: {gsheet_row.TotalCargoSlots}"
            f" ({math_floor(gsheet_row.TotalCargoSlots / 4)} WS relics)\n"
        )
        msg = msg + (f"Google Sheet bijgewerkt: {sheet_bijgewerkt}\n")
        await self._parse_module_image(
            ctx=ctx, command="info", heading=msg, userid=userid
        )


@commands.command(
    name="info",
    help=("Geeft info over een speler."),
    brief="Geeft info over een speler.",
)
async def prefix_info(self, ctx: ApplicationContext, *args):
    """Wrapper function for info, this is the command_prefix

    Args:
        ctx (ApplicationContext): Discord Context
    """
    if len(args) == 0:
        userid = ctx.author.id
    else:
        userid = self._get_userid(db=self.db, user=args[0])
    await self.info(ctx, userid)


@slash_command(
    name="info",
    description="Geeft informatie over een speler.",
    guild_ids=[717728872329642016, 390283009129054230],
)
async def slash_info(
    self,
    ctx: ApplicationContext,
    user: Option(
        str,
        description="Voor welke gebruiker wil je de informatie zien (optioneel)",
        required=False,
    ),
):
    """Wrapper function for info, this is the command_prefix

    Args:
        ctx (ApplicationContext): Discord Context
    """
    ic(ctx)
    ic(user)
    if user is None:
        userid = ctx.author.id
    else:
        userid = self._get_userid(db=self.db, user=user)
    await self.info(ctx, userid)
