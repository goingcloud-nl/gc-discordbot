import os

import discord

# from icecream import ic
# from loguru import logger
from PIL import Image, ImageDraw, ImageFont
from sqlalchemy import inspect

font = "Roboto-Bold.ttf"
font_path = "fonts/Roboto-Bold.ttf"
image_width = 100
font_size = 50
user_text_length = 400
trade_colors = (253, 242, 169)
color = {}
color["support"] = (198, 253, 169)
color["trade"] = (253, 242, 169)
color["weapon"] = (241, 185, 199)
color["mining"] = (195, 179, 250)
color["shield"] = (181, 253, 230)
color["info"] = (182, 253, 230)


def _add_text(image, command, column, text="12", font=font_path, font_size=font_size):
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype(font, size=font_size)

    p1 = (
        user_text_length
        + column * image_width
        + (round((image_width / 2)) - round(len(text * font_size) / 2)),
        (image_width - font_size) / 3,
    )
    draw.text(p1, text, fill=color[command], font=font)
    return image


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs}


def _get_module_level(db, userid, module):
    result = (
        db.session.query(db.Gsheet)
        .join(db.User, db.Gsheet.Naam == db.User.GsheetAlias)
        .filter(db.User.UserId == userid)
    )
    if result.count() == 1:
        try:
            return object_as_dict(result.first())[module]
        except:
            return ""
    else:
        return ""


def _generate_image_header(new_image, module_list):
    for index, image_dict in enumerate(module_list):
        image = "/".join(["images", list(image_dict.values())[0]])
        tmp_image = Image.open(image).convert("RGBA")
        new_image.paste(
            tmp_image,
            (user_text_length + index * image_width, 0),
        )
    # new_image.show()
    return new_image


def _generate_username_cell(new_image, username, command):
    image_size = (
        user_text_length,
        image_width,
    )
    text_image = Image.new("RGBA", image_size)

    draw = ImageDraw.Draw(text_image)
    font = ImageFont.truetype(font_path, size=font_size)
    p1 = (
        10,
        (image_width - font_size) / 3,
    )
    draw.text(p1, username, fill=color[command], font=font)
    new_image.paste(
        text_image,
        (0, 0),
    )
    return new_image


def _generate_user_row(db, user_image, command, userid, username, module_list):
    new_image = _generate_username_cell(
        new_image=user_image, username=username, command=command
    )
    for column, module in enumerate(module_list):
        module_level = _get_module_level(
            db=db, userid=userid, module=list(module.keys())[0]
        )
        if module_level is not None:
            _add_text(new_image, command=command, column=column, text=str(module_level))
    return new_image


async def _generate_wsmodule_image(self, ctx, command, ws="ws1"):
    module_list = []
    for item in self.MODULE_IMAGES[command]:
        module_list.extend(item)

    users = await self._get_rolemembers(ctx=ctx, role_name=ws)
    userids = []
    for user in users:
        userids.append(self._get_userid(db=self.db, user=str(user)))

    x_size = len(module_list)

    image_size = (
        user_text_length + x_size * image_width + 10,
        image_width * (1 + len(users)) + 10,
    )
    # empty_image = Image.new("RGBA", image_size)
    new_image = _generate_image_header(Image.new("RGBA", image_size), module_list)
    # empty_image.show()
    for row, userid in enumerate(userids):
        user_image = _generate_user_row(
            db=self.db,
            user_image=Image.new("RGBA", image_size),
            command=command,
            userid=userid,
            username=self._get_username_by_id(userid),
            module_list=module_list,
        )
        new_image.paste(
            user_image,
            (0, (row + 1) * image_width),
        )

    return new_image


async def _parse_wsmodule_image(self, ctx, command, ws):
    image = await self._generate_wsmodule_image(ctx=ctx, command=command, ws=ws)
    image_name = "-".join([str(ctx.author.id), command, ".png"])
    image.save(image_name, "PNG")
    e = discord.Embed()
    e.set_image(url=f"attachment://{image_name}")
    await ctx.channel.send(file=discord.File(image_name), embed=e)
    os.remove(image_name)
