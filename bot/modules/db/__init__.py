"""
This directory contains modules.
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker

Base = declarative_base()

from .gsheet import Gsheet
from .rsevent import RSEvent
from .rsrun_list import RSRunList
from .status import Status
from .user import User
from .wscomeback import WSComeback
from .wsentry import WSEntry
from .wstemp import WSTemp


def init(db_uri: str = "sqlite://"):
    engine = create_engine(db_uri, echo=False)
    Base.metadata.create_all(engine)
    dbsession = sessionmaker(bind=engine)
    return dbsession()
