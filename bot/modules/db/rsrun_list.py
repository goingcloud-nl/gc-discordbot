#!/usr/bin/env python


from datetime import datetime

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, Table

from . import Base


class RSRunList(Base):
    __tablename__ = "rsrun-list"
    RunId = Column(Integer, primary_key=True)
    Level = Column(Integer)
    Score = Column(Integer, default=0)
    Score_PP = Column(Integer, default=0)
    Runtime = Column(DateTime, default=datetime.now, onupdate=datetime.now)

    def __repr__(self):
        return (
            f"<RSRunList(RunId={self.RunId},"
            f"RSRunList((Level={self.Level},"
            f"RSRunList(Score={self.Level},"
            f"RSRunList(Score_PP={self.Level},"
            f"RSRunList(Runtime={self.Runtime}>"
        )
