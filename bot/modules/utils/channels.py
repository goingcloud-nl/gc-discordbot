from discord.ext import commands
from loguru import logger


async def get_channel_id_by_name(ctx: commands.Context, channel_name: str) -> int:
    """
    Get the channel id for a channel_name.

    paramters:
        channel_name:        The channel where to fetch channel_id for.
    """
    guild = ctx.guild
    all_channels = guild.channels
    for channel in all_channels:
        if channel.name == channel_name:
            return channel.id
    return 0


def get_channel_by_name(ctx: commands.Context, channel_name: str):
    """
    Get the channel for a channel_name.
    paramters:
        channel_name:        The channel where to fetch channel for.
    """
    guild = ctx.guild
    all_channels = guild.channels
    for channel in all_channels:
        if channel.name == channel_name:
            return channel
    logger.info("channel_not_found")
    return "channel_not_found"
