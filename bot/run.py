#!/usr/bin/env python
"""
The main file for the bot Robin.
"""
import configparser
import sys
from os import walk

import discord
import main as main
from discord.ext import commands
from icecream import ic
from loguru import logger
from modules import db, info, members, rs, service, utils, ws

db.session = db.init("sqlite:///data/hades.db")
try:
    with open("/.container_version", "r") as file:
        ___VERSION___ = file.read().strip()
except (FileNotFoundError, IOError):
    logger.error("version file not found")
    ___VERSION___ = "0.0.0"

config = {
    "handlers": [
        {
            "sink": sys.stdout,
            "format": ___VERSION___
            + " [{time:YYYY-MM-DD at HH:mm:ss}] [{level}]: {message}",
        }
    ],
}


def update_last_active(message):
    """Update the activity of a user

    Args:
        message (object): message sent to discord
    """
    member = message.author
    channel = message.channel
    if member.name != "Zapier":
        if member.nick is None:
            membername = member.name
        else:
            membername = member.nick
        if db.session.query(db.User).filter_by(UserId=member.id).count() == 0:
            new_user = db.User(
                UserId=member.id, DiscordAlias=membername, LastChannel=channel.name
            )
            db.session.add(new_user)
        else:
            data = {"DiscordAlias": membername, "LastChannel": channel.name}
            db.session.query(db.User).filter(db.User.UserId == member.id).update(data)

        db.session.commit()


def _get_guild_roles(bot):
    """Get all roles defined in the guild

    Args:
        bot (object): bot object

    Returns:
        dict: all roles in the dict
    """
    guild_roles = []
    for guild in bot.guilds:
        if guild.id == int(bot.settings["GUILD_ID"]):
            for role in guild.roles:
                guild_roles.append({"id": role.id, "name": role.name, "role": role})
                # logger.info(f"role.id {role.id} role.name {role.name}")
    return guild_roles


def _get_guild_channels(bot, guild_id):
    """Get all channels in the guild

    Args:
        bot (object): bot object
        guild_id (int): the id of the guild

    Returns:
        dict: all channels in the guild
    """
    guild_channels = []
    for guild in bot.guilds:
        if guild.id == int(guild_id):
            for channel in guild.channels:
                # logger.info(f"channel.name {channel.name}")
                guild_channels.append(
                    {"id": channel.id, "name": channel.name, "channel": channel}
                )
    return guild_channels


def new_bot(settings) -> discord.ext.commands.bot:
    """Create a new discordbot"""
    intents = discord.Intents.default()
    intents.members = True
    intents.message_content = True
    bot = commands.Bot(
        command_prefix=settings["COMMAND_PREFIX"],
        description=settings["BOT_DESCRIPTION"],
        intents=intents,
    )
    bot.settings = settings

    @bot.event
    async def on_message(message):
        logger.info(f"message.channel.name  _{message.channel.name}_")
        logger.info(f"message {message}")
        update_last_active(message)
        await bot.process_commands(message)

    @bot.event
    async def on_ready():
        static = {}
        static["all_guild_roles"] = _get_guild_roles(bot)
        static["all_guild_channels"] = _get_guild_channels(bot, settings["GUILD_ID"])
        static["settings"] = settings
        bot.static = static
        logger.info(f"Signed in as [{bot.user.id}] [{bot.user.name}]")

        # bot.add_cog(utils.Ping(bot=bot))
        bot.add_cog(ws.WS(bot=bot, db=db))

        # onderstaande regel uitcommentarieren
        bot.add_cog(rs.RS(bot=bot, db=db, static=static))
        # bovenstaande regel uitcommentarieren

        bot.add_cog(info.Info(bot=bot, db=db))
        bot.add_cog(service.Service(bot=bot, db=db))
        bot.add_cog(utils.GetAllRoles(bot=bot, db=db))
        bot.add_cog(members.Members(bot=bot, db=db))

    return bot


if __name__ == "__main__":
    logger.configure(**config)
    main = main.Main()
    settings = main._get_env_settings()
    logger.info("Now loading...")
    b = new_bot(settings)
    logger.info("settings loaded")
    b.run(settings["DISCORD_TOKEN"])
