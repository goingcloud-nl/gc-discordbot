import discord
from discord.ext import commands
from loguru import logger


def _run_bot(self, settings) -> discord.ext.commands.bot:

    """Create a new discordbot"""
    intents = discord.Intents.default()
    intents.members = True
    intents.message_content = True
    bot = commands.Bot(
        command_prefix=settings["COMMAND_PREFIX"],
        description=settings["BOT_DESCRIPTION"],
        intents=intents,
    )
    bot.settings = settings
    logger.info("bot loaded")

    @bot.event
    async def on_message(message):
        logger.info(f"message.channel.name  _{message.channel.name}_")
        logger.info(f"message {message}")
        self._update_last_active(message)
        await bot.process_commands(message)

    @bot.event
    async def on_ready():
        static = {}
        static["all_guild_roles"] = _get_guild_roles(bot)
        static["all_guild_channels"] = _get_guild_channels(bot)
        static["settings"] = _get_settings()
        bot.static = static
        logger.info(f"Signed in as [{bot.user.id}] [{bot.user.name}]")

        bot.add_cog(utils.Ping(bot=bot))
        # bot.add_cog(ws.Info(bot=bot, db=db))
        bot.add_cog(ws.Status(bot=bot, db=db))
        bot.add_cog(ws.Comeback(bot=bot, db=db))
        bot.add_cog(ws.Entry(bot=bot, db=db))
        bot.add_cog(ws.Idee(bot=bot, db=db))

        ##### onderstaande regel uitcommentarieren
        bot.add_cog(rs.RS(bot=bot, db=db, static=static))
        ##### bovenstaande regel uitcommentarieren

        bot.add_cog(info.Info(bot=bot, db=db))
        # bot.add_cog(home.Home(bot=bot, db=db, static=static))
        # bot.add_cog(tech.Tech(bot=bot, db=db))
        bot.add_cog(utils.GetAllRoles(bot=bot, db=db))
        bot.add_cog(members.Members(bot=bot, db=db))

    return bot
