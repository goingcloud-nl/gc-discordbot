def update_last_active(message):
    member = message.author
    channel = message.channel
    if member.name != "Zapier":
        if member.nick is None:
            membername = member.name
        else:
            membername = member.nick
        # if member.name == "RedStarQueueBot":  # enable this when the rs event is active
        #     update_rsruns(message.content)
        # logger.info(f"member {member}")
        # logger.info(f"member.id {member.id}")
        # logger.info(f"membername {membername}")
        # logger.info(f"channel.name {channel.name}")
        if db.session.query(db.User).filter_by(UserId=member.id).count() == 0:
            new_user = db.User(
                UserId=member.id, DiscordAlias=membername, LastChannel=channel.name
            )
            db.session.add(new_user)
        else:
            data = {"DiscordAlias": membername, "LastChannel": channel.name}
            db.session.query(db.User).filter(db.User.UserId == member.id).update(data)

        db.session.commit()
