"""
this is the init file for the bot
"""
import os
import sys
from re import I

from loguru import logger

___VERSION___ = "test"
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

config = {
    "handlers": [
        {
            "sink": sys.stdout,
            "format": ___VERSION___
            + " [{time:YYYY-MM-DD at HH:mm:ss}] [{level}]: {message}",
        }
    ],
}
logger.configure(**config)
