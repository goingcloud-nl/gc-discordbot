"""
testing tassadar
"""
import pytest
from mock import AsyncMock

from bot.modules.members import Members


@pytest.mark.asyncio
async def test_tassadar():
    """
    test_ping, expecting pong
    """
    members = Members()
    members._feedback = AsyncMock()
    ctx = AsyncMock()
    await members.tassadar(ctx)
    members._feedback.assert_called_once_with(
        ctx=ctx, msg="(https://www.youtube.com/watch?v=rBDwUXi1Sbw)"
    )
