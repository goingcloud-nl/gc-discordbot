"""
these tests should cover functions and classes in robin.py
"""
import pytest

from bot.modules.robin import Robin

TESTSTRING = "Dit is een teststring"


def test_sanitize():
    """
    These tests will check if the strings get sanitized properly.
    """
    robin = Robin()
    assert robin._sanitize(msg_in=TESTSTRING) == TESTSTRING
    assert (
        robin._sanitize(msg_in=TESTSTRING + "met een at @")
        == TESTSTRING + "met een at _"
    )
    assert (
        robin._sanitize(msg_in="TestString met een hash #")
        == "TestString met een hash _"
    )
    assert (
        robin._sanitize(msg_in="TestString met een lange string", maxlength=20)
        == "TestStr .. truncated"
    )
    assert robin._sanitize(msg_in="Korte TestString", maxlength=12) == " .. truncated"
